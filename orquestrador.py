import time
import datetime
from stegano import lsb
from PIL import Image
import webbrowser

def tira_foto(nome_arq_picamera):
    from picamera import PiCamera
    camera = PiCamera()
    camera.resolution = (720,480)
    camera.start_preview() #pode exibir a camera no inicio?
    time.sleep(3)
    camera.capture(str(nome_arq_picamera)+'.png')
    camera.stop_preview()

def insere_nome_e_texto():
    input_nome = input("Digite seu Nome:  ")
    input_texto = input("Digite uma frase para esconder no arquivo:  ")
    hora = datetime.datetime.now()
    gera_nome_arq = str(hora) + "_" + str(input_nome) + "_FATECAM-SI-2019"
    limpa_tela_os()
    return (input_nome,input_texto,gera_nome_arq)

def adiciona_layer(nome_arq_background,nome_arqs):
    #imagem base
    background = Image.open(nome_arq_background)
    #mascara do instagram a ser aplicada
    foreground = Image.open("mask.png")

    background.paste(foreground,(0,0), foreground)
    # background.show()
    nome_arq_stego_layer = str(nome_arqs) + "_original.png"
    background.save(str(nome_arq_stego_layer), "PNG")

    return(nome_arq_stego_layer)

def add_stego(insere_texto, caminho_arq, padrao_nome_arq):
    #aplica_stego = exifHeader.hide(caminho_arq, padrao_nome_arq + "_stego.jpg", secret_message=str(insere_texto))
    secret = lsb.hide(caminho_arq, insere_texto)
    secret.save(padrao_nome_arq + "_stego.png")

def extrai_stego(nome_arqs):
    #img_original = Image.open(nome_arqs +"_original.png" )
    #img_stego = Image.open(nome_arqs + "_stego.png")
    #img_original.show()
    #img_stego.show()
    webbrowser.open(nome_arqs +"_original.png")
    webbrowser.open(nome_arqs + "_stego.png")
    input("Pressione ENTER para mostrar o texto da foto")
    print(lsb.reveal(nome_arqs + "_stego.png"))
    #input("Pressione ENTER para enviar a foto por email")
    input("Pressione ENTER para salvar o arquivo para envio do email posteriormente")

def limpa_tela_os():
    os.system('cls' if os.name == 'nt' else 'clear')

def menu():
    print(30 * "-" , "MENU - Stego - SI - 2019" , 30 * "-")
    print("1. Tirar Foto e aplicar Estego!")
    print("2. Logs")
    print("3. Exit")
    print(87 * "-")

def salvar_para_envio_do_email(nome_arqs):
    email = input("Digite o seu email para envio:")
    arquivo = open('dados.txt','a+', encoding="utf-8")
    arquivo.write(nome_arqs + "_stego.png")
    arquivo.write(";")
    arquivo.write(email)
    arquivo.write(";")
    arquivo.write("\n")
    arquivo.close()
    print('\nDados salvos com sucesso!')

def main():
    limpa_tela_os()
    while True:
        menu()
        opt = int(input("Escolha [1-3]: "))
        if opt == 1:
            nome , texto, nome_arq = insere_nome_e_texto()
            tira_foto(nome_arq)
            nome_arq_layer_original = adiciona_layer(nome_arq+'.png',nome_arq)
            add_stego(texto,nome_arq_layer_original,nome_arq)
            extrai_stego(nome_arq)
            salvar_para_envio_do_email(nome_arq)
            #stego_confirm_ml()
            # enviar_email(nome_arq)
            # gera_log(nome_arq, nome,)
            #move_fotos( pastas: arquivos pi camera, originais e stegos)
        elif opt == 2:
            print("Gerar Logs")
        elif opt == 3:
            print ("Saindo!")
            time.sleep(1)
            break
        else:
            input("Opcao errada. Aperte Enter e tente novamente...")

if __name__ == "__main__":
    main()
